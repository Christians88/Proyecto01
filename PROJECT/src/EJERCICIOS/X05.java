package EJERCICIOS;

import javax.swing.JOptionPane;

public class X05 {
	public static void main(String[] args){
		String numero=JOptionPane.showInputDialog("Introduce un n�mero");
		int numInt=Integer.parseInt(numero);
		
		if (numInt%2==0){
			JOptionPane.showMessageDialog(null, "El n�mero "+numInt+ " es divisible por 2");
		}else{
			JOptionPane.showMessageDialog(null, "El n�mero "+numInt+ " no es divisible por 2");
		}
		}
}
